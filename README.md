<article class="markdown-body entry-content container-lg" itemprop="text"><h1 tabindex="-1" id="user-content-teos-portfolio" dir="auto"><a class="heading-link" href="#teos-portfolio">hoonhee92's portfolio<svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></h1>
<h2 tabindex="-1" id="user-content-pushpin-목차" dir="auto"><a class="heading-link" href="#pushpin-목차">📌 목차<svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 </ul>0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.8https://user-images.githubusercontent.com/119651889/235159873-3e1d74a8-b8f4-4578-8e94-008f0af3075c.png3 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></h2>
<blockquote>
<p dir="auto">📝 <a href="#Intro">Intro</a> <br>
🔨 <a href="#Skill">Skill</a> <br>
💬 <a href="#Contact">Conatct</a> <br>
✨ <a href="#Projects">Projects</a> <br>
🔎 <a href="#Etc">Etc</a> <br></p>
</blockquote>
<h2 tabindex="-1" id="user-content-pushpin-intro" dir="auto"><a class="heading-link" href="#pushpin-intro">📌 Intro</a><a id="user-content-intro"></a><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></h2>
<p dir="auto"><a target="_blank" rel="noopener noreferrer nofollow" href="https://gitlab.com/uploads/-/system/user/avatar/15268436/avatar.png?width=400"><img src="https://gitlab.com/uploads/-/system/user/avatar/15268436/avatar.png?width=400" width="300" style="max-width: 100%;"></a> <br></p>
<ul>
<li>최종학위: 경희사이버대학교 IT·디자인융합학부 AI사이버보안전공 </li>
<li>자격증: 정보처리산업기사 (필기) </li>
<li>교육활동: 하이미디어아카데미 Java, SpringBoot (23.06.21 ~ 23.11.15, 800시간)</li>
</ul>
<br>
<h2 tabindex="-1" id="user-content-pushpin-skill-" dir="Skill"><a class="heading-link" href="#pushpin-skill-">📌 Skill </a><a id="user-content-skill"></a><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></h2>
<ul dir="auto">
<li>프론트엔드: HTML / CSS / JavaScript / Vue.js / JQuery / Flutter <br></li>
<li>백엔드: Spring / SpringBoot / RESTful API(JSON) <br></li>
<li>언어: Java, Dart <br></li>
<li>DB: PostgreSQL <br></li>
<li>서버: docker, GCP, Kubernetes <br></li>
<li>Tool: IntelliJ / Git / Datagrip / Swagger <br></li>
<li>etc: google_map_API / Spring.io <br></li>
</ul>
<br>
<h2 tabindex="-1" id="user-content-pushpin-contact" dir="auto"><a class="heading-link" href="#pushpin-contact">📌 Contact</a><a id="user-content-contact"></a><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 https://user-images.githubusercontent.com/119651889/235159873-3e1d74a8-b8f4-4578-8e94-008f0af3075c.png0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></h2>
<ul dir="auto">
<li>이메일: <a href="hooulra@gmail.com">hooulra@gmail.com</a></li>
<li>블로그: <a href="https://coding-zenith.tistory.com" rel="nofollow">https://coding-zenith.tistory.com/</a></li>
<li>깃랩: <a href="https://gitlab.com/hooulra">https://gitlab.com/hooulra</a></li>
</ul>
<br>
<h3 tabindex="-1" id="user-content-1-mes" dir="auto"><a class="heading-link" href="#1-mes">1. </a><a href="https://gitlab.com/hooulra/fullgoing">Fullgoing</a><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></h3>
<blockquote>
<p dir="auto">전동 킥보드 대여 어플리케이션 개발 (팀 프로젝트) <br>
개발 기간: 2023.08.03 ~ 2023.09.26</p>
<p dir="auto">기술 스택:<br>
Java 17 / flutter / swagger / gcp / docker / Spring / SpringBoot <br>
<p dir="auto"><a href="https://gitlab.com/hooulra/fullgoing_document/-/blob/main/README.md?ref_type=heads">프로젝트 상세 설명</a> 참고</p>
</blockquote>
<hr>
<h3 tabindex="-1" id="user-content-2-tenneeds" dir="auto"><a class="heading-link" href="#2-tenneeds">2. </a><a href="https://gitlab.com/hooulra/storemanager">무인점포관리</a><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></h3>
<blockquote>
<p dir="auto"> 무인점포관리 앱 (개인 프로젝트)<br>
개발 기간: 2023.07.17 ~ 2023.07.24</p>
<p dir="auto">기술 스택:<br>
java / intellij / git / swagger <br>
<p dir="auto"><a href="https://gitlab.com/hooulra/storemanager">프로젝트 상세 설명</a> 참고</p>
</blockquote>
</blockquote>
<hr>
<h3 tabindex="-1" id="user-content-2-tenneeds" dir="auto"><a class="heading-link" href="#2-tenneeds">3. </a><a href="https://gitlab.com/hooulra/storemanager">SEEYOU COFFEE MANAGER</a><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></h3>
<blockquote>
<p dir="auto"> 커피 프렌차이즈 관리 웹, 앱 (팀 프로젝트)<br>
개발 기간: 2023.10.16 ~ 2023.11.15 예정</p>
<p dir="auto">기술 스택:<br>
Java 17 / flutter / swagger / gcp / docker / Spring / SpringBoot / vue.js <br>
<p dir="auto"><a href="https://gitlab.com/hooulra/seeyou_coffee">프로젝트 상세 설명</a> 참고</p>
</blockquote>

<br>

<h2 tabindex="-1" id="user-content-pushpin-intro" dir="auto"><a class="heading-link" href="#pushpin-intro">📌 Etc</a><a id="user-content-intro"></a><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.00ate public 변경2 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></h2>

<ul>
<li>경력사항: 투데이 엔터테인먼트, 위에화 엔터테인먼트, 빅플래닛메이드엔터 신인개발팀 / 2016.11 ~ 2023.6 (6년 근무)</li>
<li>기타 자격증: 운전면허 1종 보통 </li>
</ul>

</article>
